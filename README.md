MovingLocal
---

Moving Local is a lead generation directory website. With proper SEO, it aims to rank for specific keywords in every city within the USA with a population over 5,000 people in order to generate long-distance and short-distance moving leads.

I created this website/theme on Wordpress for the built-in CMS functionality and Yoast SEO plugin.