<?php

// check if the flexible content field has rows of data
if( have_rows('page_group') ):

     // loop through the rows of data
    while ( have_rows('page_group') ) : the_row();

        // check current row layout
        if( get_row_layout() == 'sources' ): ?>

            <?php include 'infograph_flex/sources.php'; ?>

        <?php elseif( get_row_layout() == 'table_of_contents' ): ?>

            <?php include 'infograph_flex/table-of-contents.php'; ?>

        <?php elseif( get_row_layout() == 'circle_graph_section' ): ?>

            <?php include 'infograph_flex/circle-graph-section.php'; ?>

        <?php elseif( get_row_layout() == 'two_chart_section' ): ?>

            <?php include 'infograph_flex/two-chart-section.php'; ?>

        <?php elseif( get_row_layout() == 'links' ): ?>

            <?php include 'infograph_flex/links.php'; ?>

        <?php elseif( get_row_layout() == 'main_section_1' ): ?>

            <?php include 'infograph_flex/main-section-1.php'; ?>

        <?php elseif( get_row_layout() == 'numbers' ): ?>

            <?php include 'infograph_flex/numbers.php'; ?>
                
        <?php elseif( get_row_layout() == 'list' ): ?>
                
            <?php include 'infograph_flex/list.php'; ?>
                
        <?php elseif( get_row_layout() == 'text_links' ): ?>
                
            <?php include 'infograph_flex/text-links.php'; ?>
        
        <?php elseif( get_row_layout() == 'title_text' ): ?>
                
            <?php include 'infograph_flex/title-text.php'; ?>
                
        <?php elseif( get_row_layout() == 'editor' ): ?>
            <?php $gcounter++; ?>
            <div class = "text-padding" id = "chapter-<?php echo $gcounter; ?>">
                    <!-- Needle Exchange -->
                    <div class = "uk-position-relative uk-text-center uk-margin-large-top uk-margin-medium-bottom">
                        <div class = "dots-across"></div>
                        <h2 class = "uk-cover-background ribbon-header-center color-white"><?php the_sub_field('title'); ?></h2>
                    </div>
                    <div class = "responsive-map-container">
                        <?php the_sub_field('editor'); ?>
                    </div>
            </div>

       <?php endif;

    endwhile;

else :

    // no layouts found

endif;

?>
                            