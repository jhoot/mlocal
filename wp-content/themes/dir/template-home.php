<?php /* Template Name: Home */ ?>
<section id="fold" class="overlay-1-light shadow-below overlay" style="background-image: url('/mlocal/wp-content/uploads/2018/09/AdobeStock_109688118.jpeg'); background-position: 50% 50%; background-size: cover;">
    <div class="uk-block-large">
        <div class="gridl uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 uk-width-medium-1-2 left-block uk-text-left uk-text-center-medium">
                <h3 class="color-1"><?php the_field('sub_header'); ?></h3>
                <h1 class="color-white"><?php the_field('header'); ?></h1>
                <a href="<?php the_field('button_link'); ?>" class="big-btn btn-2-hover"><?php the_field('button_text'); ?></a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-2 right-block">
                <div class="wrapper shadow">
                    <div class="right-header uk-text-center">
                        <h2 class="color-white">Find a Moving Company In</h2>
                    </div>
                    <div class="right-body">
                        <div class="uk-grid uk-grid-collapse uk-text-center">
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/alabama/"><i class="uk-icon-map-marker"></i> Alabama</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Alaska/"><i class="uk-icon-map-marker"></i> Alaska</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Arizona/"><i class="uk-icon-map-marker"></i> Arizona</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Arkansas/"><i class="uk-icon-map-marker"></i> Arkansas</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/California/"><i class="uk-icon-map-marker"></i> California</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Colorado/"><i class="uk-icon-map-marker"></i> Colorado</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Connecticut/"><i class="uk-icon-map-marker"></i> Connecticut</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Delaware/"><i class="uk-icon-map-marker"></i> Delaware</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Florida/"><i class="uk-icon-map-marker"></i> Florida</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Georgia/"><i class="uk-icon-map-marker"></i> Georgia</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Hawaii/"><i class="uk-icon-map-marker"></i> Hawaii</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Idaho/"><i class="uk-icon-map-marker"></i> Idaho</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Illinois/"><i class="uk-icon-map-marker"></i> Illinois</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Indiana/"><i class="uk-icon-map-marker"></i> Indiana</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/iowa/"><i class="uk-icon-map-marker"></i> Iowa</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Kansas/"><i class="uk-icon-map-marker"></i> Kansas</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Kentucky/"><i class="uk-icon-map-marker"></i> Kentucky</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Louisiana/"><i class="uk-icon-map-marker"></i> Louisiana</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Maine/"><i class="uk-icon-map-marker"></i> Maine</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Maryland/"><i class="uk-icon-map-marker"></i> Maryland</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Massachusetts/"><i class="uk-icon-map-marker"></i> Massachusetts</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Michigan/"><i class="uk-icon-map-marker"></i> Michigan</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Minnesota/"><i class="uk-icon-map-marker"></i> Minnesota</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Mississippi/"><i class="uk-icon-map-marker"></i> Mississippi</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Missouri/"><i class="uk-icon-map-marker"></i> Missouri</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Montana/"><i class="uk-icon-map-marker"></i> Montana</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Nebraska/"><i class="uk-icon-map-marker"></i> Nebraska</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Nevada/"><i class="uk-icon-map-marker"></i> Nevada</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/New-Hampshire/"><i class="uk-icon-map-marker"></i> New Hampshire</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/New-Jersey/"><i class="uk-icon-map-marker"></i> New Jersey</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/New-Mexico/"><i class="uk-icon-map-marker"></i> New Mexico</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/New-York/"><i class="uk-icon-map-marker"></i> New York</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/North-Carolina/"><i class="uk-icon-map-marker"></i> North Carolina</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/North-Dakota/"><i class="uk-icon-map-marker"></i> North Dakota</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Ohio/"><i class="uk-icon-map-marker"></i> Ohio</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Oklahoma/"><i class="uk-icon-map-marker"></i> Oklahoma</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Oregon/"><i class="uk-icon-map-marker"></i> Oregon</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Pennsylvania/"><i class="uk-icon-map-marker"></i> Pennsylvania</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Rhode-Island/"><i class="uk-icon-map-marker"></i> Rhode Island</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/South-Carolina/"><i class="uk-icon-map-marker"></i> South Carolina</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/South-Dakota/"><i class="uk-icon-map-marker"></i> South Dakota</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Tennessee/"><i class="uk-icon-map-marker"></i> Tennessee</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Texas/"><i class="uk-icon-map-marker"></i> Texas</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Utah/"><i class="uk-icon-map-marker"></i> Utah</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Vermont/"><i class="uk-icon-map-marker"></i> Vermont</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Virginia/"><i class="uk-icon-map-marker"></i> Virginia</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Washington/"><i class="uk-icon-map-marker"></i> Washington</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/West-Virginia/"><i class="uk-icon-map-marker"></i> West Virginia</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Wisconsin/"><i class="uk-icon-map-marker"></i> Wisconsin</a>
                            </div>
                            <div class="uk-width-1-2 uk-width-medium-1-4">
                                <a class="color-white" href="/mlocal/in/Wyoming/"><i class="uk-icon-map-marker"></i> Wyoming</a>
                            </div>
                        </div>
                    </div>
                    <div class="right-footer uk-text-center">
                        <span class="color-white">Or</span>
                        <a href="#" class="big-btn btn-2-hover">Call Now For a FREE Quote</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="home-text" class="uk-block">
    <div class="uk-grid gridl uk-grid-collapse">
        <div class="uk-width-1-1 uk-width-medium-1-2 left-block uk-text-center-medium uk-text-left">
            <h2 class="color-1"><?php the_field('left_header'); ?></h2>
            <h3 class="color-2"><?php the_field('left_sub_header'); ?></h3>
            <?php echo the_field('left_copy'); ?>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-2 right-block uk-text-center">
            <img src="<?php the_field('right_image'); ?>" alt="moving boxes">
        </div>
    </div>
</section>

<section id="home-first" class="shadow-above" style="background-image: url('/mlocal/wp-content/uploads/2018/09/inflicted.png'); background-repeat: repeat; background-position: 50% 50%;">
    <div class="gridl">
        <div id="home-first__header" class="uk-block uk-text-center">
            <h2 class="color-1">Moving Resources</h2>
        </div>
        <div id="home-first__body" class="uk-grid uk-grid-collapse uk-text-center">
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource One</h3>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource Two</h3>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource Three</h3>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource Four</h3>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource Five</h3>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="uk-width-1-1 uk-width-medium-1-3">
                    <a href="#">
                        <div class="shadow single-item overlay overlay-1-hover"  style="background-image: url('/mlocal/wp-content/uploads/2018/09/pexels-photo-1439373-1.jpeg'); background-position: 50% 50%; background-size: cover;">
                            <div class="bg-1 shadow-below">
                                <h3 class="color-white">Resource Six</h3>
                            </div>
                        </div>
                    </a>
            </div>
        </div>
    </div>
</section>

<section id="home-cta" class="uk-block bg-1 shadow-below">
    <div class="gridl uk-grid uk-grid-collapse">
        <div class="uk-width-1-1 uk-width-medium-6-10">
            <h2 class="color-white"><?php the_field('cta_header'); ?></h2>
            <p class="large color-white"><?php the_field('cta_copy'); ?></p>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-10"></div>
        <div class="uk-width-1-1 uk-width-medium-3-10 cta uk-text-right uk-text-center-medium">
            <a href="<?php the_field('cta_button_link'); ?>" class="xtra-big-btn btn-2-hover"><i class="uk-icon-phone"></i> <?php the_field('cta_button_text'); ?></a>
        </div>
    </div>
</section>

<section id="home-map" class="uk-block">
    <div class="gridl">
        <div id="home-map__header" class="uk-text-center">
            <h2 class="color-1">Find a Moving Company</h2>
            <h4 class="color-1">Click on a state to find moving companies local to you!</h4>
        </div>
        <div id="home-map__mapcontainer" class="uk-text-center">
            <?php build_i_world_map(1); ?>
        </div>
    </div>
</section>

<section id="home-bg" class="uk-block shadow-above overlay overlay-1" style="background-image: url('/mlocal/wp-content/uploads/2018/09/action-asphalt-auto-193667.jpg'); background-attachment: fixed; background-size: cover; background-position: fixed;">
    <div class="gridl">
        <div id="home-bg__header" class="uk-text-center">
            <h2 class="color-white"><?php the_field('blurbs_header'); ?></h2>
        </div>
        <?php if(have_rows('blurbs')): ?>
        <div id="home-bg__body" class="uk-grid uk-grid-collapse">
            <?php while(have_rows('blurbs')): the_row(); ?>
                <div class="uk-width-1-1 single-block uk-width-medium-1-3 uk-text-left uk-text-center-medium">
                    <div class="inner-container shadow">
                        <h3 class="color-1"><?php the_field('title'); ?></h3>
                        <?php echo the_field('copy'); ?>
                        <a href="<?php the_field('button_link'); ?>" class="big-btn btn-1-hover"><?php the_field('button_text'); ?></a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>