<?php

$phone = (get_option('phone') ? get_option('phone') : '888888888');

function gr($array) {
    return $array[array_rand($array)];
}

// A
$amountSub = array('amount', 'extent', 'number', 'supply');
$avoidSub = array('avoid', 'bypass');
$alwaysSub = array('always', 'consistently', 'regularly');
$allowsSub = array('allows', 'gives', 'provides');
// B
$bewareStartSub = array('Beware', 'Be cautious', 'Be wary');
$benefitsSub = array('benefits', 'up-sides', 'advantages');
$beforeSub = array('before', 'prior to', 'ahead of');
// C
$challengingSub = array('a challenge', 'challenging', 'difficult', 'quite difficult');
$choosingSub = array('choosing', 'shopping for', 'selecting');
$choosingStartSub = array('Choosing', 'Shopping for', 'Selecting');
$circumstancesSub = array('circumstances of', 'situations surrounding');
$checkSub = array('check', 'view', 'look at', 'analyze');
$choiceSub = array('choice', 'decision');
$companySwitch = array('move yourself or hire a moving company', 'hire a moving company or move yourself');
// D
$dueToSub = array('due to', 'because of');
$dontSub = array('don\'t', 'do not');
// E
$easySub = array('easy', 'not difficult', 'not hard', 'pretty easy');
$extraSub = array('extra', 'additional', 'further', 'supplemental');
$everyAspectSub = array('everything', 'every aspect of the move', 'every detail surrounding the move');
// F
$findingStartSub = array('Finding', 'Coming across', 'Locating');
$findingSub = array('finding', 'locating', 'using');
$feesSub = array('fees', 'charges', 'costs');
$feeSub = array('fee', 'cost', 'charge', 'total');
// G
// H
$hiddenSub = array('hidden', 'non-upfront', 'non-apparent');
// I
$includesSub = array('includes', 'comes with', 'also includes', 'also comes with');
$inAdditionStartSub = array('In addition to', 'Along with');
// J
// K
// L
$largestSub = array('largest', 'biggest');
$lookoutSub = array('on the lookout for', 'vigilantly looking for', 'watching for');
$largeSub = array('large', 'considerable', 'giant', 'hefty', 'excessive');
// M
$mostSub = array('most', 'many', 'a lot of');
$maynotSub = array('may not', 'might not', 'possibly may not', 'possibly might not');
$moreDifficultSub = array('much harder', 'much more difficult', 'much more challenging');
// N
$nowSub = array('now', 'today');
$nextSub = array('your next move', 'your move', 'your options');
// O
$overwhelmedSub = array('overwhelmed', 'stressed', 'strained');
$optionsSub = array('options', 'choices');
$oneSub = array('one', 'a choice', 'a decision');
// P
$placingSub = array('placing', 'putting');
$possessionsSub = array('possessions', 'belongings', 'furnishings', 'goods');
$professionalsSub = array('professionals', 'experts', 'experienced professionals');
// Q
// R
$reviewsSwitch = array('reviews and complaints', 'complaints and reviews', 'reviews', 'complaints');
// S
$searchingSub = array('searching', 'looking');
$someSub = array('some', 'a few');
$safelySub = array('safely', 'carefully');
$sureSub = array('sure', 'positive', 'certain');
$shouldntSub = array('shouldn\'t', 'should not');
$shouldntSub = array('shouldn\'t', 'should not');
// T
$trustworthySub = array('trustworthy', 'reputable', 'realiable', 'dependable', 'honest');
$localtoyouSub = array('that is local to you', 'nearby', 'close to you', 'in your area');
$trickedSub = array('tricked', 'deceived', 'mislead');
$twentySub = array('20-30%', '20%', '30%');
$takeCareSub = array('take care of', 'handle');
// U
// V
// W
$wontSub = array('won\'t', 'will not', 'don\'t', 'do not');
// X
// Y
// Z

$oneEnd = ' ' . gr($choosingStartSub) . ' a moving company can be ' . gr($challengingSub) . ' but we can help you, <a href="tel:'.$phone.'">call us today at ' . $phone . '</a> for a free quote.';
$twoEnd = ' Make ' . gr($sureSub) . ' that you are in good hands, <a href="tel:'.$phone.'">call us today at ' . $phone . '</a>.';
$threeEnd = ' Call ' . gr($nowSub) . ' to speak to a professional about ' . gr($nextSub) . '!';

$totalEnd = array($oneEnd, $twoEnd, $threeEnd);

$h1 = "How to Find a " . $post_title . " Moving Company";
$one = array(
    ' ' . gr($findingStartSub) . ' a ' . gr($trustworthySub) . ' moving company ' . gr($localtoyouSub) . ' can be ' . gr($challengingSub) . '.',
    ' It is ' . gr($easySub) . ' to become ' . gr($overwhelmedSub) . ' when ' . gr($searchingSub) . ' for a moving company ' . gr($dueToSub) . ' the ' . gr($amountSub) . ' of ' . gr($optionsSub) . ' out there.',
    ' ' . gr($bewareStartSub) . ' of ' . gr($extraSub) . ' ' . gr($feesSub) . ' when ' . gr($choosingSub) . ' a moving company, as ' . gr($someSub) . ' companies have ' . gr($extraSub) . ' ' . gr($feesSub) . ' depending on the ' . gr($circumstancesSub) . ' your move.'
);

$h2 = "Choosing the Right Moving Company in " . $post_title;
$two = array(
    ' One of the ' . gr($largestSub) . ' ' . gr($benefitsSub) . ' to ' . gr($findingSub) . ' a moving company ' . gr($localtoyouSub) . ' is that you can ' . gr($avoidSub) . ' being ' . gr($trickedSub) . ' by companies who ' . gr($dontSub) . ' have your best interests in mind.',
    ' You should ' . gr($alwaysSub) . ' ' . gr($checkSub) . ' a company\'s ' . gr($reviewsSwitch) . ' ' . gr($beforeSub) . ' ' . gr($placingSub) . ' your trust in them to ' . gr($safelySub) . ' move your ' . gr($possessionsSub) . ' from one place to another.',
    ' Make ' . gr($sureSub) . ' that you are ' . gr($lookoutSub) . ' companies that are charging a ' . gr($largeSub) . ' amount of the total ' . gr($feeSub) . ' as a deposit, ' . gr($mostSub) . ' ' . gr($trustworthySub) . ' companies ' . gr($wontSub) . ' charge more than ' . gr($twentySub) . ' as a deposit.'
);

$h3 = "Use a " . $post_title . " Moving Company or Move Myself";
$three = array(

    ' Making the ' . gr($choiceSub) . ' to either ' . gr($companySwitch) . ' is ' . gr($oneSub) . ' that you ' . gr($shouldntSub) . ' take lightly.',
    ' Moving yourself ' . gr($allowsSub) . ' control over ' . gr($everyAspectSub) . ', but ' . gr($includesSub) . ' a lot of ' . gr($hiddenSub) . ' ' . gr($feesSub) . ' that you ' . gr($maynotSub) . ' have thought of.',
    ' ' . gr($inAdditionStartSub) . ' hidden ' . gr($feesSub) . ', moving yourself is so ' . gr($moreDifficultSub) . ' than hiring ' . gr($professionalsSub) . ' to ' . gr($takeCareSub) . ' the process for you.'
);

?>