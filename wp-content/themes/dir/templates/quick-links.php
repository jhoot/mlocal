<?php if( have_rows('links') ): ?>
	<section class = "quick-links small-padding">
		<div class = "gridl">
			<h4 class = "display-small-small uk-text-center-medium uk-display-inline-block uk-margin-right">Quick Links</h4>
			<?php while( have_rows('links') ): the_row(); ?>
				<a href = "<?php the_sub_field('link'); ?>" title = "<?php the_sub_field('text'); ?>" class = "uk-margin-bottom display-small-small rect-btn btn-1-outline font0 uk-margin-right"><?php the_sub_field('text'); ?></a>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>