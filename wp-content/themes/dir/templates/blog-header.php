<?php use Roots\Sage\Titles; ?>

<?php $background = (get_the_post_thumbnail_url('wide') == '' ) ? get_the_post_thumbnail_url() : 'http://pinnaclerecoveryut.com/wp-content/uploads/2016/11/bg2.jpg'; ?>


<div class="uk-block-extra-large uk-cover-background" style = "background-image: url('<?php echo $background; ?>'); background-attachment: fixed;">
	    <section class="uk-small-block bg-5 slant slant-bottom">
    <div class="gridl uk-text-center">
      <div class="uk-grid uk-flex uk-flex-middle" style = "transform: rotate(1.5deg);">
        <div class="uk-width-medium-1-1">
          <h1 class = "color-white uk-display-block uk-margin-bottom-remove"><?php the_title(); ?></h1>
          <?php include 'templates/breadcrumbs.php'; ?>
        </div>
      </div>
    </div>
</section>
<section class="uk-block slant">

</section>

	  
</div>

