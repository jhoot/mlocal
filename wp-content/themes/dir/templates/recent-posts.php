<!-- 
<div class = "uk-grid uk-grid-medium uk-grid-width-1-1">
	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => '3'
			);
		$the_query =  new WP_Query($args);
		if( $the_query->have_posts() ):
			while( $the_query->have_posts() ): $the_query->the_post(); ?>
				<li class = "uk-margin-bottom">
					<div class = "uk-grid ">
					  	<div class = "uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1">
					  		<?php the_post_thumbnail('large')?>
					  	</div>
					  	<div class = "uk-width-medium-2-3 uk-width-small-1-2 uk-width-1-1">
						  	<a class="entry-summary" href = "<?php the_permalink(); ?>">

						  		<h4 class = "uk-margin-top color-1"><?php the_title(); ?></h4>						  		
						  	</a>
						  	<p style = "color: #222;"><time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></p>
						  		<p><?php echo get_the_excerpt(); ?></p>
						</div>
					</div>
				</li>
				<hr class = "uk-article-divider">
			<?php endwhile;
		endif; ?>
</div> -->
<section class="bg-black" style="background: #222; padding-top: 50px;">
    <div class="gridl">
      <div class="uk-grid uk-flex uk-flex-middle">
        <div class="uk-width-medium-2-3">
          <div class="serene-header uk-text-left small-padding uk-margin-bottom">
            <h4 class="color-4">FROM THE BLOG</h4>
            <h2 class="color-white">Stay Up TO DATE</h2>
          </div>
        </div>
        <div class="uk-width-medium-1-3 uk-text-right">
          <a href="/blog/" class="uk-display-inline-block btn-white-outline big-btn">SEE ALL POSTS</a>
        </div>
      </div>      
    </div>
    <div class="uk-grid uk-grid-width-medium-1-3 uk-grid-collapse">
      <div class="overflow-hidden">
        <a href="http://localhost:8888/tour/" class="home-scale uk-text-center overlay dark-overlay giant-padding uk-cover-background uk-display-block" style="background-image: url('/wp-content/uploads/2017/11/bg-1-768x559.jpg');">
          <h3 class="color-white">This Is An Example Title</h3>
        </a>
    </div>
    <div class="overflow-hidden">
        <a href="http://localhost:8888/tour/" class="home-scale uk-text-center overlay dark-overlay giant-padding uk-cover-background uk-display-block" style="background-image: url('/wp-content/uploads/2017/11/bg1-768x512.jpg');">
          <h3 class="color-white">This Is An Example Title</h3>
      </a>
    </div>
    <div class="overflow-hidden">
        <a href="http://localhost:8888/tour/" class="home-scale uk-text-center overlay dark-overlay giant-padding uk-cover-background uk-display-block" style="background-image: url('/wp-content/uploads/2017/11/bg3-768x512.jpg');">
          <h3 class="color-white">This Is An Example Title</h3>
      </a>
    </div>
    </div>
  </section>