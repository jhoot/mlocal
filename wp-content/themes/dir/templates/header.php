<?php $phone = (get_option('phone') ? get_option('phone') : '888888888'); ?>

<header id="header" class="banner">
    <div class="wrapper uk-grid uk-grid-collapse">
        <div class="uk-width-medium-2-10 brand">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img class="logo-color" src="/mlocal/wp-content/uploads/2018/09/MOVING-LOCAL-3-01.png" alt="Moving Local Logo" />
                <img class="logo-white" src="/mlocal/wp-content/uploads/2018/09/mlocal-white-orange.png" alt="Moving Local Logo" />
                <img class="logo-color-mobile" src="/mlocal/wp-content/uploads/2018/10/icon-dark.png" alt="Moving Local Logo" />
                <img class="logo-white-mobile" src="/mlocal/wp-content/uploads/2018/10/icon.png" alt="Moving Local Logo" />
            </a>
        </div>
        <div class="uk-width-medium-6-10 uk-text-right nav">
            <?php wp_nav_menu(); ?>
        </div>
        <div class="uk-width-medium-2-10 uk-text-right cta">
            <span id="nav-cta-text">Call for moving help today!</span>
            <a id="phone-full" href="tel:<?php echo $phone; ?>" class="big-btn btn-2-hover"><i class="uk-icon-phone"></i><?php echo $phone; ?></a>
        </div>
    </div>
    <a id="offcanvastoggle" href="#offcanvas" data-uk-offcanvas><i class="uk-icon-bars"></i></a>
</header>

<div id="offcanvas" class="uk-offcanvas">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-flip shadow bg-1">
        <img class="logo-white" src="/mlocal/wp-content/uploads/2018/09/mlocal-white-orange.png" alt="Moving Local Logo" />
        <?php wp_nav_menu(); ?>
        <span class="color-white">Get a FREE moving quote now!</span>
        <a href="#" class="big-btn btn-2-hover"><i class="uk-icon-phone"></i> (866) 987-6543</a>
    </div>
</div>

<script>
$(window).scroll(function(){
    if ($(this).scrollTop() > 50) {
       $('#header').addClass('scrolled');
    } else {
       $('#header').removeClass('scrolled');
    }
});
</script>