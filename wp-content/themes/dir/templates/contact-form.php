<section class = "uk-block overlay overlay-1-dark uk-cover-background" id = "contact" style ="background-image: url('https://holisticrecoverycenters.com/wp-content/uploads/2015/07/hrc-21.jpg');">
  <div class = "gridm">
  <h2 class = "uk-margin-large-bottom color-white uk-text-center">Everything Great Starts With A First Step.  Let Serene Beginnings Help You!</h2>
  <?php echo do_shortcode('[contact-form-7 id="125" title="Footer Form"]'); ?>
  </div>
  </section>