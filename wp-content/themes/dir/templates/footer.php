<?php $phone = (get_option('phone') ? get_option('phone') : '888888888'); ?>
<section id="footer-missing" class="uk-block bg-1">
    <div class="gridl uk-text-center">
        <h3 class="color-white">Are We Missing Anything?</h3>
        <p class="large color-white">If you own or operate a moving company and can't find your business, let us know!</p>
        <a href="#" class="big-btn btn-2-hover">Contact Us Now</a>
    </div>
</section>

<section id="footer-overlaybox" class="uk-block shadow bg-white">
    <div class="container uk-text-center">
        <h2 class="color-1">Get a FREE quote for your next move!</h2>
        <a href="tel:<?php echo $phone; ?>" class="big-btn btn-1-hover">Call Us Now</a>
    </div>
</section>

<footer id="footer" class="overlay overlay-2" style="background-image: url('/mlocal/wp-content/uploads/2018/09/action-asphalt-auto-193667.jpg'); background-position: 50% 50%; background-size: cover;">
    <div id="logo-container" class="grids uk-text-center">
        <!-- <img src="/mlocal/wp-content/uploads/2018/09/MOVING-LOCAL-3-01.png" alt="moving local logo">         -->
        <h3 class="color-white">About Moving Local</h3>
        <p class="color-white large">At Moving Local, our goal is to provide anyone and everyone with a smooth moving experience by presenting a comprehensive list of companies that provide such a service.</p>
    </div>
    <div id="bottom-footer" class="bg-1 uk-grid uk-grid-collapse">
        <div class="uk-width-1-1 uk-width-medium-1-3 uk-text-left uk-text-center-medium">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="/mlocal/wp-content/uploads/2018/09/mlocal-white-orange.png" alt="moving local logo">
            </a>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3 uk-text-center">
            <p class="color-white" style="    display: inline-block;
    margin-top: 20px;">MovingLocal.org Copyright (c) 2018 All Rights Reserved.</p>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3 uk-text-right uk-text-center-medium">
            <a href="tel:<?php echo $phone; ?>"><i class="uk-icon-phone"></i></a>
            <a href="#"><i class="uk-icon-envelope"></i></a>
        </div>
    </div>
</footer>