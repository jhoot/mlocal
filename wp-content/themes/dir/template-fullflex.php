<?php/** Template Name: Full Flex */ ?>

<?php while(have_posts()) : the_post(); ?>

    <?php 
    
    if(have_rows('full_flex')):

        while(have_rows('full_flex')): the_row();

            if( get_row_layout() == 'editor' ): ?>

                <?php include('full_flex/editor.php'); ?>
            
            <?php elseif (get_row_layout() == 'left_image'): ?>

                <?php include('full_flex/left-image.php'); ?>

            <?php elseif (get_row_layout() == 'right_image'): ?>

                <?php include('full_flex/right-image.php'); ?>

            <?php elseif (get_row_layout() == 'fold'): ?>

                <?php include('full_flex/fold.php'); ?>

            <?php elseif (get_row_layout() == 'cta'): ?>

                <?php include('full_flex/cta.php'); ?>

            <?php
            
            endif;

        endwhile;
    
    else:

        // no layouts

    endif; ?>

<?php endwhile; ?>