<style>
h2{ line-height: 1 !important; margin: 0; }

ul.checklist{
	padding: 10px 0 10px 20px;	
}
ul.checklist li.check-item{
	margin-bottom: 10px;
}
ul.checklist li.check-item:before{
    font-family: 'FontAwesome';
    content: '\f00C';
    margin: 0 5px 0 -15px;
    color: #012951;
}

</style>

<?php 
			$first_char_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
			$state_title = $post->post_title;
			$index = 0;

?>			

<?php if( $post->post_parent != 0 ){ 
	
	if( get_field('parent') ){
		include 'single-state-parent.php';
	}else{ ?>
		
		<div style = "margin-top: 25px; box-shadow: 0 0 7px rgba(0,0,0,.3); background: #fff; padding: 20px;">
			<?php the_content(); ?>
		</div>
	<?php }



	
}else{ ?>

	<section id="find-fold" class="shadow uk-block overlay-1-light overlay" style="background-image: url('/mlocal/wp-content/uploads/2018/09/action-asphalt-auto-193667.jpg'); background-position: 50% 50%; background-size: cover;">
		<div class="gridl uk-text-center">
			<h2 class="color-white">Find a Moving Company in <?php echo $state_title; ?></h2>
		</div>
	</section>

	<section id="state-cta" class="bg-white shadow-below">
		<div class="uk-block">
			<div class="gridm uk-grid uk-grid-collapse bg-1 shadow">
				<div class="uk-width-1-1 uk-width-medium-1-5 uk-block left-block">
					<img src="/mlocal/wp-content/uploads/2018/09/icon.png" alt="moving local in <?php echo $state_title; ?>">
				</div>
				<div class="uk-width-1-1 uk-width-medium-4-5 uk-block right-block uk-text-center">
					<h2 class="color-white">Moving Companies Local to <?php echo $state_title; ?></h2>
					<div class = "list-padding-offset uk-text-center uk-margin-top">
						<ul class = "custom-list check uk-grid uk-grid-width-medium-1-2 uk-grid-width-1-1">
							<li style = "padding-bottom: 5px;" class = "color-white">Local Moving Companies</li>
							<li style = "padding-bottom: 5px;" class = "color-white">Long Distance Moving Companies</li>
							<li style = "padding-bottom: 5px;" class = "color-white">Storage Companies</li>
							<li style = "padding-bottom: 5px;" class = "color-white">Auto Transport Companies</li>
							<li style = "padding-bottom: 5px;" class = "color-white">Freight Shipping Companies</li>
							<li style = "padding-bottom: 5px;" class = "color-white">Logistics Companies</li>
						</ul>  
					</div>
					<div id="browsecity" class = "bg-blue padding-small uk-clearfix uk-margin-large-bottom">
						<div class = "abar-title uk-margin-bottom">
							<h3 class = "uk-text-center color-white">Browse <?php echo $state_title; ?> Moving Companies By City</h3>				
						</div>
						
						<div id="letters" style = "padding: 0 20px;">
							<div class = "abar uk-margin-bottom uk-hidden-small">			
								<?php foreach($first_char_array as $current_letter):
									$state_cities = $wpdb->get_results("SELECT post_name, city FROM cities WHERE SUBSTR(post_name,1,1) = '$current_letter' AND state = '$state_title' ORDER BY city;");
									$index++;
									if( count($state_cities) > 0 ): ?>
										<a href = "#<?php echo $current_letter; ?>cities" class = "abar-letter color-white" data-uk-smooth-scroll="{offset: 135}"><?php echo $current_letter; ?></a>
									<?php else: ?>
										<span class = "abar-letter color-g3"><?php echo $current_letter; ?></span>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
							<a href = "#letterdir" data-uk-smooth-scroll="{offset: 64}" style = "background: #ec465a !important;" title = "Browse Movers By City" class = "uk-margin-bottom uk-visible-small header-btn btn">Click To See Cities</a>
						</div>
					</div>
				</div>
				<div id="bottom-cta" class="bg-white uk-width-1-1">
					<div class="uk-grid uk-grid-collapse">
						<div class="uk-width-1-1 uk-width-medium-1-2 uk-text-center">
							<h3 class="color-1">Or call us for a FREE quote today!</h3>
						</div>
						<div class="uk-width-1-1 uk-width-medium-1-2 uk-text-center">
							<a href="#" class="big-btn btn-2-hover"><i class="uk-icon-phone"></i> Call Now</a>
						</div>
					</div>				
				</div>
			</div>
			
		</div>
	</section>

	<div class = "padding-small">
		<div class = "grid1140">
		

		<div class = "clearfix uk-margin-bottom uk-text-center uk-block">
			<h2 class = "color-1 uk-margin-bottom">Search Movers By City in <?php echo $state_title; ?></h2>
			<!-- Prints 1st Paragraph -->
			<?php if( get_field('top_paragraph') ):			
				echo '<p>' . get_field('top_paragraph') . '</p>';
			endif; ?>
		</div>
		<!-- Loop Through The Cities in the State-->
		<?php $index = 0; ?>
		<div class = "states-container" id = "letterdir">
			    <?php foreach($first_char_array as $current_letter):
			     	$state_cities = $wpdb->get_results("SELECT post_name, city FROM cities WHERE SUBSTR(post_name,1,1) = '$current_letter' AND state = '$state_title' ORDER BY city;");
					 
					 if( count($state_cities) > 0 ): ?>
					 <?php $index++; ?>		     	
			     	<div class = "single-letter padding-small background-<?php echo $index % 2; ?>" id = "<?php echo $current_letter; ?>cities">
			     		<div class = "uk-grid">
			     			<div class = "uk-width-medium-1-5 uk-visible-large uk-text-center"><span class = "letter"><?php echo $current_letter; ?></span></div>
			     			<div class = "uk-width-medium-4-5 uk-width-1-1">
						     	<ul class = "uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-3 uk-grid-width-1-2" style = "padding-left: 20px; padding-right: 20px;" id = "allCities">
								    <?php foreach($state_cities as $current_city): ?>
								    	<li class = "state-link"><a title = "<?php echo $current_city->city ?>, <?php echo $state_title ?> Moving Companies" href="/mlocal/nearby/<?php echo $current_city->post_name?>/"><?php echo $current_city->city ?></a></li>
								    <?php endforeach; ?>
							    </ul>
							</div>
					    </div>
					</div>
					<?php endif; ?>
					<!--<hr class="uk-article-divider">-->
			    <?php endforeach; ?>
			    <?php wp_reset_postdata();?>				
	    </div>					
	</div>
<?php } ?>