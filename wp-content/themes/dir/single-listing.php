<?php 


/**

 * The Template for displaying all single listings

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */
 
    $p_id = $post->ID;
    $post_title = $post->post_title;
    $listing = $wpdb->get_results("SELECT * FROM movinglistings WHERE slug = '$post->post_name';");

    
    // Get the meta data for the listing
    $address = $listing[0]->e_address;
    $city = $listing[0]->e_city; 
    $state = $listing[0]->e_state;
    $zip = $listing[0]->e_postal;
    $website = $listing[0]->web_url;   
    $phone = $listing[0]->biz_phone;
    $category = $listing[0]->cat_sub;
    $latitude = $listing[0]->latitude;
    $longitude = $listing[0]->longitude;

?>

<div id="listing-top" class="uk-block-large">

    <div class="gridl single-listing-container">
        <div class="single-listing-header shadow bg-1 uk-text-center">
            <h2 class="color-white"><?php echo $post_title; ?></h2>
            <h4 class="color-white" style="margin-top: 0;"><?php echo $category; ?></h4>
        </div>
        <div class="single-listing-body shadow bg-white">
            <div class="uk-grid uk-grid-collapse">
                <div id="details" class="uk-width-1-1 uk-width-medium-1-2">
                    <div class="listing shadow" itemscope itemtype = "http://schema.org/Organization">
                        <style type="text/css">
                            strong {
                                color:#252525;
                            }
                            img.list_banner {width:100%;}
                        </style>
                        <div class = "detail-row info-row grey-row">
                            <p><strong itemprop = "name"><?php echo $post_title ?><br/></strong><span itemprop = "description"> is located in <?php echo $city . ", " . $state . "." ?></span></p>
                        </div>
                        <div class = "detail-row address-row white-row">
                            <p><strong>Address:</strong><br/><span itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><?php echo '<span itemprop = "streetAddress">' .$address . "</span><br/>" . '<span itemprop = "addressLocality">' . $city . ", " . $state . '</span> , ' . '<span itemprop = "postalCode">' . $zip . '</span>' ?></span> </p>
                        </div>
                        <div class = "detail-row phone-row grey-row">
                            <p><strong>Phone:</strong><br/><span itemprop = "telephone"><?php echo $phone ?></span></p>
                        </div>
                        <div class="detail-row website-row white-row">
                            <p><strong>Website:</strong><br>
                                <?php if( $website != '') { ?>
                                    <a title = "<?php echo $post_title ?> Website"  target = "_blank" href="<?php echo $website ?> "><?php echo $website?></a>
                                <?php } else { echo '<p>Website not listed.</p>'; } ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div id="map" class="uk-width-1-1 uk-width-medium-1-2">
                    <iframe width="100%" height="500" frameborder="0" scrolling="no" style="border:0" src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyBJZM-32Bp7HZZvDBhv-TwIyYKOjw1u6s8&location=<?php echo $latitude; ?>,<?php echo $longitude; ?>&heading=210&pitch=10&fov=35"></iframe>
                    
                </div>
            </div>
        </div>
    </div>

</div>

<?php $movingtips = array('Get rid of unused and unnecessary items - the more than you get rid of, the easier your move will be.', 'Sort your belongings by category - sorting your belongings will make your life easier when you arrive at your new home because you will know where everything is.', 'Donate the belongings that you aren’t planning on bringing with you to your new home could find their new home with someone that needs it.', 'Sell stuff that you don’t plan on keeping, or plan on upgrading in the near future, it will decrease the amount of possessions that you need to have moved.', 'Change your address a week before you move - make sure you avoid the hassle of not having your bills, credit card statements and packages arrive on time because they were sent to your old address.', 'Use small boxes for heavier possessions - avoid having a 150 pound box that you’re going to have to cart up stairs by packing your heavier stuff in smaller boxes. Smaller boxes will make your life a lot easier throughout the entire moving process.', 'Label EVERYTHING - you can avoid banging your head against a wall after your move by making sure you take your time to appropriately label everything when packing.'); ?>

<section class="uk-block">
    <div class="gridm">
        <div id="listing-cta" class="shadow bg-1">
            <h2 class="color-white quote">Rather Just Get a Quote?</h2>
            <p class="color-white large">Call us today to get a free quote on your next move. Let us take care of the big stuff so that you can have more time to focus on what really matters to you!</p>
            <a href="#" class="big-btn btn-2-hover"><i class="uk-icon-phone"></i> Call now!</a>
        </div>
    </div>
</section>

<!-- <section id="home-bg" class="listing-under-block uk-block overlay overlay-1" style="background-image: url('/mlocal/wp-content/uploads/2018/09/action-asphalt-auto-193667.jpg'); background-attachment: fixed; background-size: cover; background-position: fixed;">
    <div class="gridl">
        <div id="home-bg__body" class="uk-grid uk-grid-collapse">
            <div class="uk-width-1-1 single-block uk-text-center">
                <div class="inner-container shadow ">
                <h2 class="color-1">Moving Can Be Stressful</h2>
                    <p class="large">It doesn't have to be. Call us today to get a quote on your next move quickly and easily so you can spend more of your time on the things that you care about!</p>
                    <a href="#" class="big-btn btn-1-hover"><i class="uk-icon-phone"></i> Call Now</a>
                </div>
            </div>
        </div>
    </div>
</section> -->



<div class="uk-block bg-white shadow" style="margin-bottom: -30px;">
    <div id="moving-tip" class="gridl">
        <h4 class="color-2">Moving Tip</h4>
        <h2 class="color-1">Did You Know?</h2>
        <p class="large"><?php echo $movingtips[array_rand($movingtips)]; ?></p>
    </div>
</div>

<?php wp_reset_postdata();?>