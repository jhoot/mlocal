<?php include 'templates/page-header.php'; ?>
  <section class="uk-small-block bg-5 slant slant-bottom">
    <div class="gridl uk-text-center">
      <div class="uk-grid uk-flex uk-flex-middle" style = "transform: rotate(1.5deg);">
        <div class="uk-width-medium-1-1">
          <h1 class = "color-white uk-display-block uk-margin-bottom-remove">The Blog</h1>
          <?php include 'templates/breadcrumbs.php'; ?>
        </div>
      </div>
    </div>
</section>
<section class="uk-block slant bg-white">
    <div class="gridl">      
        <!-- <div class = "uk-margin-bottom">
          <div class="serene-header uk-text-center">
            <h4 class="color-4">WELCOME HOME</h4>
            <h2 class="">Learn why serene is perfect for you</h2>
          </div>
        </div> -->
        <?php 
        remove_filter('the_content', 'wpautop'); ?>      
        <div data-uk-grid-parallax="{translate:200}" data-uk-grid="{gutter: 30, controls: '.selector'}" class = "uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-3 uk-flex uk-flex-middle">
        <?php    
            $counter = 1;
            while ( have_posts() ) : the_post(); ?>
              <?php $counter = ($counter == 3) ? 1 : $counter; ?>
              <div class = "roll-wrapper">
                <a href = "<?php the_permalink(); ?>" class = "single-roll overlay overlay-<?php echo $counter; ?>-hover" style = "background-image: url('<?php the_post_thumbnail_url('large'); ?>');">
                  <div class = "inner-roll">
                    <h3 class = "color-white"><?php the_title(); ?></h3>
                    <p style = "text-transform: none !important;" class = "color-white"><?php echo substr(get_the_excerpt(),0,150); ?>...</p>
                    <div class = "color-4 uk-text-bold" href = "<?php the_permalink(); ?>">Read More</div>
                  </div>
                </a>
              </div>
              <?php $counter++; ?>
            <?php endwhile; ?>
        </div>
      </div>
</section>