<?php 


/**

 * The Template for displaying all single cities

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */

?>
<style>
.uk-grid{ margin-top: 0 !important; }
</style>
<?php
	$phone = (get_option('support_phone') ? get_option('support_phone') : '888888888');
	$post_title = $post->post_title;
    $coordinates = $wpdb->get_results("SELECT * FROM cities WHERE post_title = '$post_title';");
    $latitude = $coordinates[0]->latitude;
    $longitude = $coordinates[0]->longitude;
	$closest_cities_query = "SELECT city, post_name FROM cities
									ORDER BY (ACOS(SIN(RADIANS('$latitude'))*
    								SIN(RADIANS(latitude))+
    								COS(RADIANS('$latitude'))*
    								COS(RADIANS(latitude))*
    								COS(RADIANS(longitude)-
    									RADIANS('$longitude'))) * 3959) ASC LIMIT 1,8";
    $closest_cities_listings = $wpdb->get_results($closest_cities_query);

?>	
<style>
h2{ line-height: 1 !important; margin: 0; }


	</style>

	<section id="find-fold" class="shadow uk-block overlay-1-light overlay" style="background-image: url('/mlocal/wp-content/uploads/2018/09/action-asphalt-auto-193667.jpg'); background-position: 50% 50%; background-size: cover;">
		<div class="gridl uk-text-center">
			<h2 class="color-white">Find a Moving Company in <?php echo $post_title; ?></h2>
		</div>
	</section>

	<?php
	include_once('spinnerdata.php');

	$onetotal = [
		"header" => $h1,
		"body" => $one
	];
	$twototal = [
		"header" => $h2,
		"body" => $two
	];
	$threetotal = [
		"header" => $h3,
		"body" => $three
	];

	$total = array($onetotal, $twototal, $threetotal);

	function getr($array) {
		return $array[array_rand($array)];
	}

	shuffle($total);

	shuffle($totalEnd);

	// print_r($total);

	?>

	<section id="home-text" class="uk-block">
		<div class="uk-grid gridl uk-grid-collapse">
			<div class="uk-width-1-1 uk-width-medium-1-2 left-block uk-text-center-medium uk-text-left">
				<h2 class="color-1"><?php echo $total[0]['header']; ?></h2>
				<!-- <h3 class="color-2">But it doesn't have to be!</h3> -->
				<p class="large"><?php shuffle($total[0]['body']); foreach ($total[0]['body'] as $sentence) { echo $sentence; }; echo $totalEnd[0]; ?></p>
			</div>
			<div class="uk-width-1-1 uk-width-medium-1-2 right-block uk-text-center">
				<img src="/mlocal/wp-content/uploads/2018/09/box-transparent-stack-2.png" alt="moving boxes">
			</div>
		</div>
	</section>

	<section id="home-cta" class="uk-block bg-1 shadow-below">
		<div class="gridl uk-grid uk-grid-collapse">
			<div class="uk-width-1-1 uk-width-medium-6-10">
				<h2 class="color-white">Not Sure Where To Start?</h2>
				<p class="large color-white">If you'd rather not spend the time looking for yourself, speak with one of our representatives now to get a free moving quote with a company local to you!</p>
			</div>
			<div class="uk-width-1-1 uk-width-medium-1-10"></div>
			<div class="uk-width-1-1 uk-width-medium-3-10 cta uk-text-right uk-text-center-medium">
				<a href="#" class="xtra-big-btn btn-2-hover"><i class="uk-icon-phone"></i> Call Now!</a>
			</div>
		</div>
	</section>
	
	<div id="city-bottom-content" class = "padding-small">
		<div class = "gridl" style="padding-top: 30px;">
		<div id="close-cities" class = "uk-block uk-clearfix bg-white shadow uk-margin-bottom">
			<div class = "text-padding">
				<h2 class = "uk-text-center color-1 uk-margin-bottom">Find A Moving Company In A City Near <font style = "color: #f36a24;"><?php echo $post_title; ?></font></h2>
				<div class = "uk-grid uk-grid-collapse uk-text-center uk-grid-width-medium-1-4 uk-grid-width-1-1 closest-cities-grid">
					<?php global $current_city; 
					foreach($closest_cities_listings as $current_city): ?>
						<div class = "bottom-margin"><a class = "color-1 map-before uk-margin-bottom" href = "/mlocal/near/<?php echo $current_city->post_name ?>/"><i class="uk-icon-map-marker"></i> <?php echo $current_city->city ?> </a></div>
					<?php endforeach; ?>
				</div>
			</div>		
		</div>


<?php global $globalcity; $globalcity = $post_title; ?>

<?php // include_once('spinner_data.php');

	$listings = $wpdb->get_results("SELECT *, ACOS(SIN(RADIANS('$latitude'))*SIN(RADIANS(latitude))+COS(RADIANS('$latitude'))*COS(RADIANS(latitude))*COS(RADIANS(longitude)-RADIANS('$longitude'))) * 3959 AS distance
									FROM movinglistings
									ORDER BY distance ASC	Limit 36;");


$x = 1; 
$counter = 0;
if ($listings): 

global $post, $y;

foreach ($listings as $post):
	   	// Sets up the post data
	   	setup_postdata($post);
       	$address = get_post_meta($post->ID, 'e_address', true);
       	$city = get_post_meta($post->ID, 'e_city', true); 
       	$state = get_post_meta($post->ID, 'e_state', true);
       	$zip = get_post_meta($post->ID, 'e_postal', true);
       	$website = get_post_meta($post->ID, 'web_url', true);   
		$phone = get_post_meta($post->ID, 'biz_phone', true);
		$link = str_replace(' ', '-', $post->post_title) . '-' . $post->e_city . '-' . $post->e_state;
		
		shuffle($total[1]['body']);
		$final2 = '';
		foreach ($total[1]['body'] as $sentence) { $final2 .= $sentence; };
		$final2 .= $totalEnd[1];
		shuffle($total[2]['body']);
		$final3 = '';
		foreach ($total[2]['body'] as $sentence) { $final3 .= $sentence; };
		$final3 .= $totalEnd[2];

       	// Outputs the data from the spinner
		// if ($y == 0)  echo "<h2>Drug & Alcohol Detoxification Facilities in " . $post_title . " </h2><p>" . $oneFinal . "</p>"; 
		// if ($y == 3) echo "<h2>How to Choose the Drug or Alcohol Detox Facility That’s For You</h2><p>" . $twoFinal . "</p>";
		 if ($y == 9) echo "<div class='uk-block'><div class='gridl spinner-content shadow'><h2 class='color-1'>" . $total[1]['header'] . "</h2><p>" . $final2 . "</p></div></div>";
		// if ($y == 9) echo "<h2>How Long Will Detox from Drugs and/or Alcohol Take?</h2><p>" . $fourFinal . "</p>";
		if ($y == 27) echo "<div class='uk-block'><div class='gridl spinner-content shadow'><h2 class='color-1'>" . $total[2]['header'] . "</h2><p>" . $final3 . "</p></div></div>";	
		// if ($y == 15) echo "<h2>Ready to Get Help and Change Your Life for Good?</h2><p>". $sixFinal . "</p>";
		 if ($y == 18) { ?>
			<div id="city-cta" class="gridl">
				<div class="uk-block bg-1 shadow">
					<div id="city-cta-wrapper" class="uk-grid uk-grid-collapse">
						<div class="uk-width-1-1 uk-width-medium-7-10 uk-text-left uk-text-center-medium">
							<h2 class="color-white">Too many options to choose from?</h2>
							<p class="color-white large">Stop stressing the details and let us help you so that you can spend more time on the things you care about!</p>
						</div>
						<div class="uk-width-1-1 uk-width-medium-3-10 uk-text-right uk-text-center-medium">
							<a href="#" class="big-btn btn-2-hover"><i class="uk-icon-phone"></i> Call Now</a>
						</div>
					</div>
				</div>
			</div>
		 <?php }
		//  if ($y == 24) echo "<div class='spinner-content-wrapper'><div class='gridl spinner-content shadow'><h2 class='color-1'>SPINNER CONTENT</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lacus nisl, tincidunt ac tellus non, sagittis lacinia urna. Suspendisse at varius sapien. Maecenas sed diam eget lectus porta malesuada sed eget nisi. Fusce mattis finibus tortor, quis consequat quam blandit eget. Pellentesque lacinia ligula nec velit elementum blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam congue tellus ut ex elementum volutpat non eget dui. Quisque volutpat ex ut quam tristique suscipit. In hac habitasse platea dictumst.</p></div></div>";
		 
		$y++;

		if($y == 22){
			// $is_more = true;
			// echo '<div class = "more-centers" id = "moreCenters" style = "display:none;">';
		}
		

		if ($x == 1) { ?>
			<div class="uk-grid">
		<?php } ?>
		<div class = "uk-width-medium-1-3 uk-width-1-1 listings-grid-container var-margin-bottom">
			<div class="listings-container shadow" itemscope itemtype = "http://schema.org/Organization">
				<div class = "listings-item-top">
					<span class = "uk-display-block" style="color:#000080;"><strong><a style = "font-size: 16px;" class = "color-red" itemprop = "url" href="/mlocal/company/<?php echo $link; ?>/" title = "See full listing information of <?php echo $post->post_title; ?>"><span itemprop = "name"><?php echo $post->post_title; ?></span></a></strong></span>
					<span class = "uk-display-block" itemprop = "address" itemscope itemtype = "http://schema.org/PostalAddress"><span itemprop = "streetAddress"><?php echo $post->e_address; ?></span><br/><span itemprop = "addressLocality"><?php echo $post->e_city.', '.$post->e_state; ?></span>, <span itemprop = "postalCode"><?php echo $post->e_postal; ?></span></span>
					<span class ="bottom-margin color-blue uk-display-block" style = "font-weight: bold;"><?php echo number_format((float)$post->distance, 2, '.', ''); ?> miles from the center of <?php echo $post_title; ?></span>
				</div>
				<!-- <div>
					<ul class="listings-services">
						<?php # if ($services_text2) {echo '<li class = "listing-service" itemprop = "description">'.$services_text2.'</li>';} ?>
						<?php # if ($services_text7) {echo '<li class = "listing-service">'.$services_text7.'</li>';} ?>

					</ul>
				</div> -->
			</div>
		</div>

		<?php if ($x == 3) {echo '</div>'; $x=1;} else { $x++; }
		$counter++;
		//}
	// endwhile;
?>



<?php endforeach; 
	// if($is_more){ 
	// 	echo '</div>';
	// 	echo '<div class = "more_listings_button_container" style = "width:80%; height: 40px; margin: 10px auto; margin-top: 20px; position: relative;">';
	// 	echo '<div class = "more_listings_button uk-text-center" >';
	// 	echo '<a id = "isMoreButton" class="header-btn big-btn btn-2-hover" href="">Click For More Moving Companies</a>';
	// 	echo '<a id = "isLessButton" style = "display: none;" class="header-btn btn" href="">Show Less</a>';
	// }
?>

<?php endif;  ?>

<?php wp_reset_postdata();?>

		</div>

	</div>