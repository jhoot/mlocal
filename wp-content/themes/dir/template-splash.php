<?php /** Template Name: Splash */ ?>


<?php if(is_page(74612)) { ?>
    <section id="splash-main-section" class="uk-block-large overlay overlay-1" style="background-image: url('/mlocal/wp-content/uploads/2018/10/AdobeStock_137386768.jpeg'); background-size: cover; background-position: 50% 50%;">
<?php } else { ?>
    <section id="splash-main-section" class="uk-block-large" style="background-image: url(''); background-size: cover; background-position: 50% 50%;">
<?php } ?>

</section>