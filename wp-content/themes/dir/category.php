
<?php #get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// are we on page one?
if(1 == $paged) {
    $args = array(
        'posts_per_page' => 1,
        'orderby' => 'most_recent'
        );

        $the_query = new WP_Query( $args );
		while (have_posts()) : the_post();
			if ( has_post_thumbnail() ) : ?>
			<div class = "home-hero uk-cover-background uk-block-large overlay overlay-1" style = "min-height: 0; background-image: url('<?php the_post_thumbnail_url(); ?>');">
				<div class = "gridl uk-text-center">	  	
					<a class = "featured-post" href="<?php echo get_permalink(); ?>" title = "<?php the_title(); ?>" class = "uk-overlay-scale">
						<span class = "featured color-white uk-margin-bottom-remove uk-text-bold uk-text-uppercase">Recent Article</span>
						<h1 class = "uk-margin-top-remove color-white"><?php the_title(); ?></h1>
					</a>
				</div>
			</div>							
		<?php endif;
		break;
		endwhile;
	$page_nums = 9;
}

?>

<?php $counter = 0; ?>
<div class = "uk-block">
	<div class = "gridl">
		<div class = "uk-grid">
			<div class = "uk-width-medium-2-3 uk-margin-bottom">
				<h3>From The Blog</h3>
				<?php while (have_posts()) : the_post(); ?>
				  	<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());  ?>	  
				  	<hr class = "uk-article-divider">
				<?php endwhile; ?>
				<?php the_posts_navigation(); ?>					
			</div>
			<div class = "uk-width-medium-1-3 uk-margin-bottom">
				<?php include 'templates/sidebar.php'; ?>
			</div>
		</div>
		
	</div>
</div>


