<section>
    <div class="uk-grid uk-grid-collapse">
        <div id="right-image-text" class="uk-width-1-1 bg-g1 uk-width-medium-1-2">
            <h2 class="color-1"><?php the_sub_field('title'); ?></h2>
            <?php echo get_sub_field('editor'); ?>
            <a href="<?php the_sub_field('link'); ?>" class="big-btn btn-2-hover"><?php the_sub_field('link_text'); ?></a>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-2">
            <img src="<?php the_sub_field('image'); ?>">
        </div>
        
    </div> 
</section>