<section id="fullflex-cta" class="shadow uk-block bg-<?php the_sub_field('background_color'); ?>">
    <div class="gridl uk-grid uk-grid-collapse">
        <div class="uk-width-1-1 uk-width-medium-7-10">
            <h2 class="color-<?php the_sub_field('text_color'); ?>"><?php the_sub_field('header'); ?></h2>
            <?php echo get_sub_field('editor'); ?>
        </div>
        <div class="uk-width-1-1 uk-width-medium-3-10 uk-text-right uk-text-center-medium">
            <a href="<?php the_sub_field('link'); ?>" class="big-btn btn-<?php the_sub_field('button_color'); ?>-hover"><?php the_sub_field('link_text'); ?></a>
        </div>
    </div>
</section>