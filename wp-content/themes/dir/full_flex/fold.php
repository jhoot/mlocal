<section id="fullflex-fold" class="shadow uk-block overlay-1-light overlay" style="background-image: url('<?php the_sub_field('image'); ?>'); background-position: 50% 50%; background-size: cover;">
    <div class="gridl uk-text-center">
        <h2 class="color-<?php the_sub_field('text_color'); ?>"><?php the_sub_field('header'); ?></h2>
        <h4 class="color-<?php the_sub_field('text_color'); ?>"><?php the_sub_field('sub_header'); ?></h4>
    </div>
</section>