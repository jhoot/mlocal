-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 01:42 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movinglocal`
--

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state` varchar(32) NOT NULL,
  `post_name` varchar(32) NOT NULL,
  `abbreviation` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state`, `post_name`, `abbreviation`) VALUES
('Alabama', 'alabama', 'AL'),
('Alaska', 'alaska', 'AK'),
('Arizona', 'arizona', 'AZ'),
('Arkansas', 'arkansas', 'AR'),
('California', 'california', 'CA'),
('Colorado', 'colorado', 'CO'),
('Connecticut', 'connecticut', 'CT'),
('Delaware', 'delaware', 'DE'),
('Florida', 'florida', 'FL'),
('Georgia', 'georgia', 'GA'),
('Hawaii', 'hawaii', 'HI'),
('Idaho', 'idaho', 'ID'),
('Illinois', 'illinois', 'IL'),
('Indiana', 'indiana', 'IN'),
('Iowa', 'iowa', 'IA'),
('Kansas', 'kansas', 'KS'),
('Kentucky', 'kentucky', 'KY'),
('Louisiana', 'louisiana', 'LA'),
('Maine', 'maine', 'ME'),
('Maryland', 'maryland', 'MD'),
('Massachusetts', 'massachusetts', 'MA'),
('Michigan', 'michigan', 'MI'),
('Minnesota', 'minnesota', 'MN'),
('Mississippi', 'mississippi', 'MS'),
('Missouri', 'missouri', 'MO'),
('Montana', 'montana', 'MT'),
('Nebraska', 'nebraska', 'NE'),
('Nevada', 'nevada', 'NV'),
('New Hampshire', 'new-hampshire', 'NH'),
('New Jersey', 'new-jersey', 'NJ'),
('New Mexico', 'new-mexico', 'NM'),
('New York', 'new-york', 'NY'),
('North Carolina', 'north-carolina', 'NC'),
('North Dakota', 'north-dakota', 'ND'),
('Ohio', 'ohio', 'OH'),
('Oklahoma', 'oklahoma', 'OK'),
('Oregon', 'oregon', 'OR'),
('Pennsylvania', 'pennsylvania', 'PA'),
('Rhode Island', 'rhode-island', 'RI'),
('South Carolina', 'south-carolina', 'SC'),
('South Dakota', 'south-dakota', 'SD'),
('Tennessee', 'tennessee', 'TN'),
('Texas', 'texas', 'TX'),
('Utah', 'utah', 'UT'),
('Vermont', 'vermont', 'VT'),
('Virginia', 'virginia', 'VA'),
('Washington', 'washington', 'WA'),
('West Virginia', 'west-virginia', 'WV'),
('Wisconsin', 'wisconsin', 'WI'),
('Wyoming', 'wyoming', 'WY');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`post_name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
