<?php
/*
* CPT STUFF
*/
add_action( 'init', 'create_post_type' );
function create_post_type() {
 $labels = array(
        'name' => _x('States', 'states'),
        'singular_name' => _x('State', 'state'),
        'add_new' => _x('New State', 'book'),
        'add_new_item' => __('Add new state'),
        'edit_item' => __('Edit state'),
        'new_item' => __('New state'),
        'view_item' => __('View state'),
        'search_items' => __('Search states'),
        'not_found' =>  __('State not found.'),
        'not_found_in_trash' => __('There are no states in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'States'
      );
    
  register_post_type( 'state',
    array(
      'labels' => $labels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true, 
      'query_var' => true,
      'rewrite' => array(
      'slug'=>'in',
      'with_front'=> false),
      'capability_type' => 'post',
      'has_archive' => false, 
      'hierarchical' => true,
      'menu_position' => null,
      'supports' => array('title','editor','thumbnail','custom-fields','page-attributes'),
    )
  );
  $labels = array(
        'name' => _x('Listings', 'listings'),
        'singular_name' => _x('listing', 'listing'),
        'add_new' => _x('New Listing', 'book'),
        'add_new_item' => __('Add new listing'),
        'edit_item' => __('Edit listing'),
        'new_item' => __('New listing'),
        'view_item' => __('View listing'),
        'search_items' => __('Search listings'),
        'not_found' =>  __('Listing not found.'),
        'not_found_in_trash' => __('There are no listings in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Listings'
      );
    
  register_post_type( 'listing',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array(
        'slug'=>'company',
        'with_front'=> false),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => true,
        'menu_position' => null,
  'supports' => array('title','editor','thumbnail','custom-fields'),
    )
  );
  $labels = array(
        'name' => _x('Cities', 'cities'),
        'singular_name' => _x('city', 'cities'),
        'add_new' => _x('New City', 'book'),
        'add_new_item' => __('Add new city'),
        'edit_item' => __('Edit city'),
        'new_item' => __('New city'),
        'view_item' => __('View city'),
        'search_items' => __('Search city'),
        'not_found' =>  __('City not found.'),
        'not_found_in_trash' => __('There are no cities in the trash.'), 
        'parent_item_colon' => '',
        'menu_name' => 'Cities'
      );
  register_post_type( 'city',
    array(
      'labels' => $labels,
      'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array(
        'slug'=>'near',
        'with_front'=> false),
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => true,
        'menu_position' => null,
  'supports' => array('title','editor','thumbnail','custom-fields'),
    )
  );

  add_post_type_support( 'page', 'custom-fields' );
}
