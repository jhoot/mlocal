<?php 
/*
Plugin Name: Stodzy Wordpress Unplugged
Version: 1.0
Plugin URI: http://www.stodzyinternetmarketing.com
Description: A wonderul, little plugin developed by Stodzy to jumpstart custom Wordpress theme development.
Author: Stodzy Internet Marketing
Author URI: http://www.stodzyinternetmarketing.com/
*/


if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


require_once(__DIR__ . '/swu-cpt.php');
require_once(__DIR__ . '/swu-widgets.php');
require_once(__DIR__ . '/swu-theme-options.php');
require_once(__DIR__ . '/swu-functions.php');

