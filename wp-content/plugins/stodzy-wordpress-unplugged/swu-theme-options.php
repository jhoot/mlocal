<?php
/*
* THEME OPTIONS MUTHAFUCKA
*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Stodzy Unplugged General Settings',
		'menu_title'	=> 'Stodzy Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}




/* Enqueues additional code */
add_action('wp_head', 'swu_enqueue_additional_code');
function swu_enqueue_additional_code(){
	if( have_rows('additional_code', 'option') ){
		while( have_rows('additional_code', 'option') ){
			the_row();
			if( get_sub_field('facebook_pixel') != '' ){ 
				the_sub_field('facebook_pixel');	
			}
			if( get_sub_field('google_analytics') != '' ){ 
				the_sub_field('google_analytics');	
			}
			if( get_sub_field('header_code') != '' ){ 
				the_sub_field('header_code');	
			}
			if( get_sub_field('footer_code') != '' ){ 
				the_sub_field('footer_code');	
			}
		}
	}
}


/* Sets up main option global variables */
add_action('init', 'swu_set_main_variables');
function swu_set_main_variables(){
	if( have_rows('main_site_options', 'option') ){
		global $swu_desktop_logo, $swu_mobile_logo, $swu_about_us, $swu_about_image, $swu_address1, $swu_address2, $swu_city, $swu_state, $swu_zip, $swu_phone, $swu_phone_stripped, $swu_fax;
		while( have_rows('main_site_options', 'option') ){
			the_row();
			if( get_sub_field('desktop_logo') != '' ){ 
				$swu_desktop_logo = get_sub_field('desktop_logo');	
			}
			if( get_sub_field('mobile_logo') != '' ){ 
				$swu_mobile_logo = get_sub_field('mobile_logo');	
			}
			if( get_sub_field('about_us') != '' ){ 
				$swu_about_us = get_sub_field('about_us');	
			}
			if( get_sub_field('about_image') != '' ){ 
				$swu_about_image = get_sub_field('about_image');	
			}
			if( get_sub_field('address1') != '' ){ 
				$swu_address1 = get_sub_field('address1');	
			}
			if( get_sub_field('address2') != '' ){ 
				$swu_address2 = get_sub_field('address2');	
			}
			if( get_sub_field('city') != '' ){ 
				$swu_city = get_sub_field('city');	
			}
			if( get_sub_field('state') != '' ){ 
				$swu_state = get_sub_field('state');	
			}
			if( get_sub_field('zip') != '' ){ 
				$swu_zip = get_sub_field('zip');	
			}
			if( get_sub_field('phone_number') != '' ){ 
				$swu_phone = get_sub_field('phone_number');	
				$swu_phone_stripped = preg_replace('/\D+/', '', $swu_phone);
			}
			if( get_sub_field('') != '' ){ 
				$swu_fax = get_sub_field('');	
			}
		}
	}
}


/* Enqueues additional code */
add_action('wp_head', 'swu_set_social_media');
function swu_set_social_media(){
	global $swu_social_media;
	$swu_social_media = [];
	if( have_rows('social_media', 'option') ){
		$counter = 0;
		while( have_rows('social_media', 'option') ){
			the_row();
			$swu_social_media[$counter]['platform'] = get_sub_field('platform');
			$swu_social_media[$counter]['icon'] = get_sub_field('icon');
			$swu_social_media[$counter]['link'] = get_sub_field('link');
		}
	}
}


/* Deals with custom post types */
add_action('init', 'swu_set_cpt');
function swu_set_cpt(){
	$custom_post_types = get_field('custom_post_types','option');
	if( $custom_post_types ):
		foreach( $custom_post_types as $cpt ):
			$function_name =  'swu_create_' . $cpt;
			$function_name();
		endforeach; 
	endif; 			
}