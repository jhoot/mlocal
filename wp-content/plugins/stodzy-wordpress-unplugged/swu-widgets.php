<?php
/*
* WIDGET STUFF
*/


add_action( 'widgets_init', 'my_widget_init' );

function my_widget_init() {
    register_widget( 'silo_widget' );
}
// Lets add in the typical custom post types clients love to see :)


// Widget Time!
class silo_widget extends WP_Widget
{

  /**
   * Holds widget settings defaults, populated in constructor.
   *
   * @var array
   */
  protected $defaults;

  /**
   * Constructor. Set the default widget options and create widget.
   *
   * @since 0.1.8
   */
  function __construct() {

    $this->defaults = array(
      'title'           => '',
      'page_id'         => '',
      'show_image'      => 0,
      'image_alignment' => '',
      'image_size'      => '',
      'show_title'      => 0,
      'show_content'    => 0,
      'content_limit'   => '',
      'more_text'       => '',
    );

    $widget_ops = array(
      'classname' => 'silo_widget',
            'description' => 'Widget For Sidebar Silos'
    );
    

    parent::__construct( 'silo_widget', 'Stodzy Sidebar Silo', $widget_details );

  }

  // Echoes the widget
  function widget( $args, $instance ) {

    global $wp_query;

    //* Merge with defaults
    $instance = wp_parse_args( (array) $instance, $this->defaults );

    echo $args['before_widget'];

    //* Set up the author bio
    if ( ! empty( $instance['title'] ) ):
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) . $args['after_title'];
    else:
      echo $args['before_title'] . get_the_title($the_ID) . $args['after_title'];
    endif;

    global $post;

      $the_ID = get_the_ID();
      if ( is_page() && $post->post_parent ) {          
        $the_ID = $post->post_parent;
      }     
      $args = array(
                  'post_type' => 'page',//it is a Page right?
                  'post_status' => 'publish',
                  'post_parent' => $the_ID,
                  'posts_per_page' => -1,
                  'orderby'        => 'menu_order'              
                  );
        $the_query = new WP_Query( $args );
        if( $the_query->have_posts() ): ?>
                <ul class="custom-nav"> 
                <?php while($the_query->have_posts() ): $the_query->the_post();  ?>
                      <li class="menu-item first">
                        <a href="<?php the_permalink(); ?>" title = "<?php the_title(); ?>" data-wpel-link="internal"><span><?php the_title(); ?></span></a>
                      </li>                 
                 <?php endwhile; ?>
                </ul>
        <?php endif; wp_reset_postdata();
    
  }

  function update( $new_instance, $old_instance ) {

    $new_instance['title']     = strip_tags( $new_instance['title'] );
    $new_instance['more_text'] = strip_tags( $new_instance['more_text'] );
    return $new_instance;

  }

  // Backend Form
  function form( $instance ) {

    //* Merge with defaults
    $instance = wp_parse_args( (array) $instance, $this->defaults );

    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'genesis' ); ?>:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
    </p>

    
    <?php

  }

}